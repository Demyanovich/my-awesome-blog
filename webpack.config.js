var webpack = require('webpack');
var path = require('path');

var APP_DIR = path.resolve(__dirname, 'app');

var config = {
	devtools: 'eval-source-map',
	entry: APP_DIR + '/index.js',
	output: {
		filename: 'bundle.js'
	},
	module : {
		loaders : [
			{
				test : /\.js?/,
				include : APP_DIR,
				loader : 'babel'
			}
		]
	}
};

module.exports = config;