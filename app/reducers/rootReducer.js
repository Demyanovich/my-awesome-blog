import { GET_POSTS, EDIT_POST, DELETE_POST, GET_COMMENTS } from '../actions/actionTypes';
import remove from 'lodash/remove';
import find from 'lodash/find';
import undoable, { distinctState } from 'redux-undo';

const initialState = {
	posts: []
};

export default undoable((state = initialState, action) => {
	let posts;
	let post;

	switch (action.type) {
		case GET_POSTS:
			return { posts: action.posts };
		case EDIT_POST:
			posts = Object.assign([], state.posts);
			post = find(posts, (post) => post.id === action.postId);
			post.title = action.state.title;
			post.body = action.state.body;
			return { posts };
		case DELETE_POST:
			posts = Object.assign([], state.posts);
			remove(posts, (post) => post.id === action.postId);
			return { posts };
		case GET_COMMENTS:
			posts = Object.assign([], state.posts);
			post = find(posts, (post) => post.id === action.postId);
			post.comments = action.comments;
			return { posts };
		default:
			return state;
	}
}, { filter: distinctState() })
