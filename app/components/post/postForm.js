import React from 'react';
import classNames from 'classnames';

const PostForm = ({ title, body, loading, errors, onChange, editPost, cancelEdit }) => (
	<form onSubmit={editPost}>
		<div className={classNames('form-group', { 'has-error': errors.title })}>
			<label className="control-label">Title</label>
			<input
				type="text"
				name="title"
				value={title}
				onChange={onChange}
				className="form-control"
				disabled={loading}
			/>
			{ errors.title && <span className="help-block">{errors.title}</span> }
		</div>
		<div className={classNames('form-group', { 'has-error': errors.body })}>
			<label className="control-label">Body</label>
			<textarea
				value={body}
				name="body"
				onChange={onChange}
				className="form-control"
				disabled={loading}
			/>
			{ errors.body && <span className="help-block">{errors.body}</span> }
		</div>
		<div className="btn-toolbar">
			<input
				type="button"
				value="Cancel"
				className="btn btn-default"
				onClick={cancelEdit}
				disabled={loading}
			/>
			<input
				type="submit"
				value="Submit"
				className="btn btn-primary"
				disabled={loading}
			/>
		</div>
	</form>
);

PostForm.propTypes = {
	title: React.PropTypes.string.isRequired,
	body: React.PropTypes.string.isRequired,
	loading: React.PropTypes.bool.isRequired,
	errors: React.PropTypes.object.isRequired,
	onChange: React.PropTypes.func.isRequired,
	editPost: React.PropTypes.func.isRequired,
	cancelEdit: React.PropTypes.func.isRequired
};

export default PostForm;