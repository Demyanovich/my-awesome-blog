import React from 'react';

const PostView = ({ data, commentsVisible, loading, showEditForm, deletePost, getComments }) => (
	<div>
		<h2>{data.title}</h2>
		<div>{data.body}</div>

		<div className="btn-toolbar">
			<input
				type="button"
				value="Edit"
				onClick={showEditForm}
				disabled={loading}
				className="btn btn-primary"
			/>
			<input
				type="button"
				value="Delete"
				onClick={deletePost}
				disabled={loading}
				className="btn btn-danger offset3"
			/>
			<input
				type="button"
				value={commentsVisible ? 'Hide comments' : 'Show comments' }
				onClick={getComments}
				disabled={loading}
				className="btn btn-default pull-right"
			/>
		</div>
		{ commentsVisible && data.comments.map(comment =>
			<p key={comment.id}><em>{comment.body}</em></p>
		)}
	</div>
);

PostView.propTypes = {
	data: React.PropTypes.object.isRequired,
	commentsVisible: React.PropTypes.bool.isRequired,
	loading: React.PropTypes.bool.isRequired,
	showEditForm: React.PropTypes.func.isRequired,
	deletePost: React.PropTypes.func.isRequired,
	getComments: React.PropTypes.func.isRequired
};

export default PostView;