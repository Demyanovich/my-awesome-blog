import React from 'react';
import validatePost from '../validation/validatePost';
import { connect } from 'react-redux';
import PostForm from './postForm';
import PostView from './postView';
import { editPost, deletePost, getComments } from '../../actions/postActions';

class Post extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			title: this.props.data.title,
			body: this.props.data.body,
			commentsVisible: false,
			isEditMode: false,
			errors: {}
		};

		this.showEditForm = this.showEditForm.bind(this);
		this.cancelEdit = this.cancelEdit.bind(this);
		this.editPost = this.editPost.bind(this);
		this.deletePost = this.deletePost.bind(this);
		this.getComments = this.getComments.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	showEditForm() {
		this.setState({ isEditMode: true });
	}

	cancelEdit() {
		this.setState({
			title: this.props.data.title,
			body: this.props.data.body,
			isEditMode: false
		});
	}

	editPost(e) {
		e.preventDefault();

		const { errors, valid} = validatePost(this.state);
		this.setState({ errors });
		if (!valid) {
			return;
		}

		this.setState({ loading: true });
		this.props.editPost(this.props.data.id, this.state).then(() => {
			this.setState({
				loading: false,
				isEditMode: false
			});
			// todo: 'setTimeout' does not work
			setTimeout(alert('Post has been updated.'), 0);
		}).catch(error => {
			this.setState({
				loading: false,
				isEditMode: false
			});
			// todo: 'setTimeout' does not work
			setTimeout(alert(error), 0);
		});
	}

	deletePost() {
		const result = confirm('Are you sure?');
		if (!result) {
			return;
		}

		this.setState({ loading: true });
		this.props.deletePost(this.props.data.id).then(() =>
			setTimeout(() => alert('Post has been deleted.'), 0)
		).catch(error => {
			this.setState({ loading: false });
			setTimeout(() => alert(error), 0)
		});
	}

	getComments() {
		if (this.props.data.comments) {
			this.setState({ commentsVisible: !this.state.commentsVisible })
		} else {
			this.setState({ loading: true });
			this.props.getComments(this.props.data.id).then(() => {
				this.setState({
					commentsVisible: true,
					loading: false
				});
			}).catch(error => {
				this.setState({ loading: false });
				setTimeout(() => alert(error), 0)
			});
		}
	}

	onChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	render() {
		return (
			this.state.isEditMode
			? <PostForm
				title={this.state.title}
				body={this.state.body}
				loading={this.state.loading}
				errors={this.state.errors}
				onChange={this.onChange}
				editPost={this.editPost}
				cancelEdit={this.cancelEdit}
			/>
			: <PostView
				data={this.props.data}
				commentsVisible={this.state.commentsVisible}
				loading={this.state.loading}
				showEditForm={this.showEditForm}
				deletePost={this.deletePost}
				getComments={this.getComments}
			/>
		);
	}
}

Post.propTypes = {
	data: React.PropTypes.object.isRequired
};

export default connect(null, { getComments, editPost, deletePost })(Post);