import React from 'react';
import { render } from 'react-dom';
import Post from './post';
import { connect } from 'react-redux';
import { getPosts } from '../../actions/postActions';
import { ActionCreators as UndoActionCreators } from 'redux-undo';

class PostsList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: true
		};

		this.props.getPosts().then(() => {
			this.setState({ loading: false });
		}).catch(error => {
			this.setState({ loading: false });
			alert(error);
		});
	}

	render() {
		return (
			<div className="container">
				<h1>My Awesome Blog</h1>
				<div className="btn-toolbar">
					<input
						type="button"
						value="Undo"
						onClick={this.props.undo}
						disabled={!this.props.canUndo}
						className="btn btn-default"
					/>
					<input
						type="button"
						value="Redo"
						onClick={this.props.redo}
						disabled={!this.props.canRedo}
						className="btn btn-default"
					/>
				</div>
				{ this.state.loading
					? 'Loading...'
					: this.props.posts.map(post =>
						<Post
							key={post.id}
							data={post}
						/>
					)
				}
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		posts: state.present.posts,
		canUndo: state.past.length > 0,
		canRedo: state.future.length > 0
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		getPosts: () => dispatch(getPosts()),
		undo: () => dispatch(UndoActionCreators.undo()),
		redo: () => dispatch(UndoActionCreators.redo())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(PostsList);
