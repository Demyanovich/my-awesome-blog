export default {
	required: field => `${field} is required.`
}
