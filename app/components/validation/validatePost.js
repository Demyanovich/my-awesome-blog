import validator from 'validator';
import errorMessages from '../shared/errorMessages';
import isEmpty from 'lodash/isEmpty';

export default function (data) {
	let errors = {};

	if (validator.isEmpty(data.title)) {
		errors.title = errorMessages.required('Title');
	}

	if (validator.isEmpty(data.body)) {
		errors.body = errorMessages.required('Body');
	}

	return {
		errors,
		valid: isEmpty(errors)
	}
}