import { EDIT_POST, DELETE_POST, GET_POSTS, GET_COMMENTS } from './actionTypes';
import fetch from 'isomorphic-fetch';
import { API_ENDPOINT, MAX_PAGE_SIZE } from '../components/shared/constants';

export function getPosts() {
	return dispatch => {
		return fetch(`${API_ENDPOINT}/posts?_limit=${MAX_PAGE_SIZE}`)
			.then(response => {
				if (!response.ok) {
					throw('An error occurred while getting post');
				}
				return response.json();
			})
			.then(posts => {
				dispatch({
					type: GET_POSTS,
					posts
				});
			}).catch(error => {
				throw(error);
			});
	};
}

export function editPost(postId, state) {
	return dispatch => {
		return fetch(`${API_ENDPOINT}/posts/${postId}`, {
			method: 'PUT',
			data: {
				id: postId,
				title: state.title,
				body: state.body
			}
		}).then((response) => {
			if (!response.ok) {
				throw('An error occurred while updating post');
			}
			dispatch({
				type: EDIT_POST,
				postId,
				state
			});
		}).catch(error => {
			throw(error);
		});
	};
}

export function deletePost(postId) {
	return dispatch => {
		return fetch(`${API_ENDPOINT}/posts/${postId}`, { method: 'DELETE' })
			.then((response) => {
				if (!response.ok) {
					throw('An error occurred while deleting post');
				}
				dispatch({
					type: DELETE_POST,
					postId
				});
			}).catch(error => {
				throw(error);
			});
	};
}

export function getComments(postId) {
	return dispatch => {
		return fetch(`${API_ENDPOINT}/posts/${postId}/comments`)
			.then(response => {
				if (!response.ok) {
					throw('An error occurred while getting post');
				}
				return response.json();
			})
			.then(comments => {
				dispatch({
					type: GET_COMMENTS,
					postId,
					comments
				});
			}).catch(error => {
				throw(error);
			});
	};
}
